package projetosefaz.modelo.beans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import projetosefaz.modelo.usuario.Usuario;
import projetosefaz.modelo.usuario.UsuarioRepositorio;

@ManagedBean
public class ListarUsuarioBeans {
	UsuarioRepositorio UR = new UsuarioRepositorio();
	@EJB
	private List<Usuario> usuarios;

	public List<Usuario> getUsuarios() {
		usuarios = UR.listarUsuarios();
		return usuarios;
	}

}
