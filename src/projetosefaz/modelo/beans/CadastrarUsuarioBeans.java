package projetosefaz.modelo.beans;

import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import projetosefaz.controlador.fachada.FachadaControlador;
import projetosefaz.modelo.usuario.Usuario;

@ManagedBean
public class CadastrarUsuarioBeans {
	@EJB
	private Usuario usuario;

	public Usuario getUsuario() {
		if (usuario == null) {
			usuario = new Usuario();
		}
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String cadastrarUsuario() {
		Usuario emailExistente = FachadaControlador.getInstanciaUsuario().verificarExistenciaEmail(usuario.getEmail());
		if (emailExistente == null) {
			Date dataAtual = new Date();
			usuario.setData(dataAtual);
			FachadaControlador.getInstanciaUsuario().cadastrarUsuario(usuario);

			usuario = new Usuario();
		}else {
		FacesContext.getCurrentInstance().addMessage("msgValidador", 
				new FacesMessage("J� existe um cadastro com esse email."));
		return null ;
		}
		return "principal.xhtml?faces-redirect=true";
	}

	public String cadastrarInicial() {
		Date dataAtual = new Date();
		usuario.setData(dataAtual);
		FachadaControlador.getInstanciaUsuario().cadastrarUsuario(usuario);

		usuario = new Usuario();
		return "login.xhtml?faces-redirect=true";
	}
}
