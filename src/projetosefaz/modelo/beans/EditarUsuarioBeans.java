package projetosefaz.modelo.beans;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.PrimeFaces;

import projetosefaz.controlador.fachada.FachadaControlador;
import projetosefaz.controlador.usuario.SessionContext;
import projetosefaz.modelo.usuario.Usuario;

@ManagedBean
public class EditarUsuarioBeans {

	private Usuario usuario;
	private int idUsuarioSelecionado;
	private String nomeSelecionado;
	private String emailSelecionado;
	private String senhaSelecionado;
	private String numeroSelecionado;
	private String tipoSelecionado;
	private Date dataSelecionado;
	private int telefoneSelecionado;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public int getTelefoneSelecionado() {
		return telefoneSelecionado;
	}

	public void setTelefoneSelecionado(int telefoneSelecionado) {
		this.telefoneSelecionado = telefoneSelecionado;
	}

	public int getIdUsuarioSelecionado() {
		return idUsuarioSelecionado;
	}

	public void setIdUsuarioSelecionado(int idUsuarioSelecionado) {
		this.idUsuarioSelecionado = idUsuarioSelecionado;
	}

	public String getNomeSelecionado() {
		return nomeSelecionado;
	}

	public void setNomeSelecionado(String nomeSelecionado) {
		this.nomeSelecionado = nomeSelecionado;
	}

	public String getEmailSelecionado() {
		return emailSelecionado;
	}

	public void setEmailSelecionado(String emailSelecionado) {
		this.emailSelecionado = emailSelecionado;
	}

	public String getSenhaSelecionado() {
		return senhaSelecionado;
	}

	public void setSenhaSelecionado(String senhaSelecionado) {
		this.senhaSelecionado = senhaSelecionado;
	}

	public String getNumeroSelecionado() {
		return numeroSelecionado;
	}

	public void setNumeroSelecionado(String numeroSelecionado) {
		this.numeroSelecionado = numeroSelecionado;
	}

	public String getTipoSelecionado() {
		return tipoSelecionado;
	}

	public void setTipoSelecionado(String tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
	}

	public Date getDataSelecionado() {
		return dataSelecionado;
	}

	public void setDataSelecionado(Date dataSelecionado) {
		this.dataSelecionado = dataSelecionado;
	}

	public String redirecioneEditar() {

		String id = SessionContext.getInstance().getParametroId("idUsuario");
		 int idUsuario = Integer.parseInt(id);

		List<Usuario> usuarios = FachadaControlador.getInstanciaUsuario().listarUsuarios();
		for (Usuario usuario : usuarios) {

			if (usuario.getId() == idUsuario) {
				
				idUsuarioSelecionado =  usuario.getId() ;
				nomeSelecionado = usuario.getNome();
				emailSelecionado = usuario.getEmail();
				senhaSelecionado = usuario.getSenha();
				telefoneSelecionado = usuario.getTelefone();
				tipoSelecionado = usuario.getTipoTelefone();
				dataSelecionado = usuario.getData();
			}
		}

		PrimeFaces.current().executeScript("$('.editarUsuario').modal()");

		return null;
	}

	public void editarUsuario() {
		
		Usuario usuario = new Usuario ();
		usuario.setData(dataSelecionado);
		usuario.setNome(nomeSelecionado);
		usuario.setEmail(emailSelecionado);
		usuario.setTelefone(telefoneSelecionado);
		usuario.setTipoTelefone(tipoSelecionado);
		usuario.setSenha(senhaSelecionado);
		usuario.setId(idUsuarioSelecionado);
		
		FachadaControlador.getInstanciaUsuario().atualizarUsuario(usuario);

	}
	public String redirecioneDetalhes() {
		String id = SessionContext.getInstance().getParametroId("idUsuario");
		int idUsuario = Integer.parseInt(id);

		List<Usuario> usuarios = FachadaControlador.getInstanciaUsuario().listarUsuarios();
		for (Usuario usuario: usuarios) {

			if (usuario.getId() == idUsuario) {

				idUsuarioSelecionado =  usuario.getId() ;
				nomeSelecionado = usuario.getNome();
				emailSelecionado = usuario.getEmail();
				senhaSelecionado = usuario.getSenha();
				telefoneSelecionado = usuario.getTelefone();
				tipoSelecionado = usuario.getTipoTelefone();
				dataSelecionado = usuario.getData();

			}
		}

		PrimeFaces.current().executeScript("$('.usuarioDetalhe').modal()");
		return null;

	}


}
