package projetosefaz.controlador.fachada;

import java.util.List;

import projetosefaz.controlador.usuario.UsuarioControlador;
import projetosefaz.modelo.usuario.Usuario;
import projetosefaz.modelo.usuario.UsuarioInterface;

public class FachadaControlador implements UsuarioInterface {

	private static UsuarioControlador usuarioInstancia;

	FachadaControlador() {
		usuarioInstancia = new UsuarioControlador();

	}

	public static UsuarioControlador getInstanciaUsuario() {
		if (usuarioInstancia == null) {
			usuarioInstancia = new UsuarioControlador();
		}
		return usuarioInstancia;
	}

	@Override
	public void cadastrarUsuario(Usuario u) {
		usuarioInstancia.cadastrarUsuario(u);

	}

	@Override
	public Usuario loginAcesso(String email, String senha) {
		return usuarioInstancia.loginAcesso(email, senha);
	}

	@Override
	public void removerUsuario(int idUsuario) {
		usuarioInstancia.removerUsuario(idUsuario);
	}

	@Override
	public void atualizarUsuario(Usuario u) {
		usuarioInstancia.atualizarUsuario(u);
	}

	@Override
	public List<Usuario> listarUsuarios() {
		return usuarioInstancia.listarUsuarios();
	}

	@Override
	public Usuario verificarExistenciaEmail(String email) {
		// TODO Auto-generated method stub
		return usuarioInstancia.verificarExistenciaEmail(email);
	}

}
