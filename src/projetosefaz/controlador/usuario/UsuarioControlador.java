package projetosefaz.controlador.usuario;

import java.util.List;

import projetosefaz.modelo.fachada.FachadaRepositorio;
import projetosefaz.modelo.usuario.Usuario;
import projetosefaz.modelo.usuario.UsuarioInterface;

public class UsuarioControlador implements UsuarioInterface {

	private static FachadaRepositorio repositorio;

	public UsuarioControlador() {
		repositorio = new FachadaRepositorio();
	}

	@Override
	public void cadastrarUsuario(Usuario u) {
		repositorio.cadastrarUsuario(u);
	}

	@Override
	public Usuario loginAcesso(String email, String senha) {
		return repositorio.loginAcesso(email, senha);
	}

	@Override
	public void removerUsuario(int idUsuario) {
		repositorio.removerUsuario(idUsuario);
	}

	@Override
	public void atualizarUsuario(Usuario u) {
		repositorio.atualizarUsuario(u);
	}

	@Override
	public List<Usuario> listarUsuarios() {
		return repositorio.listarUsuarios();
	}
	public Usuario verificarExistenciaEmail(String email) {
		return repositorio.verificarExistenciaEmail(email);
	}

}
